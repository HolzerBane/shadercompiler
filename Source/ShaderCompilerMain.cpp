#include "..\RapidXML\rapidxml.hpp"
#include <string>
#include <fstream>
#include <sstream>
#include <d3dx10async.h>
#include <iostream>

// config xml and other literals
const char* XmlRootTag				= "Shaders";
const char* XmlShadersOutputPathTag = "OutputPath";
const char* XmlShaderTag			= "Shader";
const char* XmlShaderVersionTag		= "Version";
const char* XmlShaderPathTag		= "Path";
const char* XmlShaderFileTag		= "File";
const char* ShaderSuffix			= "_o";
const char* LogSeparator			= "-------------------------------------------------------------------------------";

// breaks the DX error so every message type starts in a new line
std::string breakDXError( char* dxErrorData, size_t dxErrorLength )
{
	std::string errorStr( dxErrorData, dxErrorLength );
	// TODO : Visual Studio output puts an EXEC string at the beginning of the line for no apparent reason...
	errorStr.insert( errorStr.find(': '), "\n" );
	return errorStr;
}

int main(int argc, char* argv[])
{
	// defaults
	std::string shaderOutputPath = ".\\Output\\";
	std::string basePath		 = ".\\";
	std::string configFilePath	 = "config.xml";
	std::string configFileData;

	// logging
	std::ofstream logStream( "log.txt", std::ios::out );
	unsigned errors		= 0;
	unsigned warnings	= 0;
	unsigned successes	= 0;

	// get location of the config file if available
	if ( argc == 2 )
	{
		configFilePath = argv[1];
		basePath = configFilePath.substr( 0, configFilePath.rfind('\\') + 1 );
	}

	// load the config file's contents
	std::ifstream file( configFilePath, std::ios::in );
	if(file.is_open())
	{
		std::stringstream ss;
		ss << file.rdbuf();
		configFileData = ss.str();

		file.close();
	}
	else
	{
		std::cout << "Config file not found: " << configFilePath << std::endl;
		file.close();
		return -1;
	}

	// parse the config xml
	rapidxml::xml_document<> doc;
	char* textStart = &configFileData[0];
	doc.parse<0>(textStart);

	// header
	std::cout << LogSeparator << std::endl;
	std::cout << "Shader Compiler" << std::endl;
	std::cout << LogSeparator << std::endl;
	std::cout << "Compiling started using " << configFilePath << std::endl;
	std::cout << LogSeparator << std::endl;

	// load output path if available
	auto rootNode = doc.first_node( XmlRootTag );
	auto outputNode = rootNode->first_node( XmlShadersOutputPathTag );
	if ( outputNode )
	{
		shaderOutputPath = outputNode->value();
	}

	// create necessary directory if it doesn't exist
	shaderOutputPath = basePath + shaderOutputPath;
	DWORD attribs = GetFileAttributes(shaderOutputPath.c_str());
	if ( !( attribs & FILE_ATTRIBUTE_DIRECTORY ) || attribs == INVALID_FILE_ATTRIBUTES )
	{
		if ( !CreateDirectory( shaderOutputPath.c_str(), 0 ) )
		{
			logStream << "Cannot create output path: " << shaderOutputPath << std::endl;
			return -1;
		}
	}

	// run through XmlShaderTag
	for ( auto shaderNode = rootNode->first_node( XmlShaderTag ); shaderNode != nullptr; shaderNode = shaderNode->next_sibling( XmlShaderTag ) )
	{
		char* shaderVersion = shaderNode->first_node( XmlShaderVersionTag )->value();
		char* shaderPath = "";
		auto pathNode =  shaderNode->first_node( XmlShaderPathTag );
		if ( pathNode )
		{
			shaderPath = pathNode->value();
		}
		 
		// run through File XmlShaderFileTag
		for ( auto fileNode = shaderNode->first_node( XmlShaderFileTag ); fileNode != nullptr; fileNode = fileNode->next_sibling( XmlShaderFileTag ))
		{
			ID3D10Blob* shaderData = nullptr;
			ID3D10Blob* errorData = nullptr;
			std::string fullFilePath = basePath + shaderPath + fileNode->value();
			
			std::cout << "Compiling shader '" << fileNode->value() << "'" << std::endl;
			
			// compile the shader to blob
			HRESULT result = D3DX10CompileFromFile( fullFilePath.c_str(), nullptr, nullptr, nullptr, shaderVersion, 0, 0, nullptr, &shaderData, &errorData, nullptr );
	
			if ( result != S_OK )
			{
				errors++;

				if ( errorData )
				{
					char* compileErrors = (char*)(errorData->GetBufferPointer());
					unsigned bufferSize = errorData->GetBufferSize();
					std::string dxError = breakDXError(compileErrors, bufferSize);
					logStream << "Error: compiling shader " << fullFilePath << std::endl;
					logStream << dxError << std::endl;
					logStream << LogSeparator << std::endl;
					std::cout << dxError << std::endl;
				}
				else
				{
					std::cout << "File not found at: " << fullFilePath.c_str() << std::endl;
					logStream << "File not found at: " << fullFilePath.c_str() << std::endl;		
				}
			}
			else
			{
				if ( errorData )
				{
					warnings++;
					std::cout << breakDXError((char*)(errorData->GetBufferPointer()), errorData->GetBufferSize()) << std::endl;
				}
				else
				{
					successes++;
				}

				// write blob to output file
				std::string outputFileName( shaderOutputPath );
				outputFileName += fileNode->value();
				outputFileName += ShaderSuffix;
				std::ofstream shaderObjStream( outputFileName.c_str(), std::ios::out | std::ios::binary );
				shaderObjStream.write( (char*)shaderData->GetBufferPointer(), shaderData->GetBufferSize() );
				shaderObjStream.flush();
				shaderObjStream.close();

				std::cout << "Shader '" << fileNode->value() << "' compiled to '" << outputFileName << "'"<< std::endl;
			}
			std::cout << LogSeparator << std::endl;
		}
	}
	
	std::cout << LogSeparator << std::endl;
	std::cout << "Compiling finished. Total files: " << errors + warnings + successes << std::endl;
	std::cout << "number of files with errors: " << errors << std::endl;
	std::cout << "number of files with warnings: " << warnings << std::endl;
	std::cout << "number of files successful: " << successes << std::endl;
	std::cout << LogSeparator << std::endl;
	
	logStream.flush();
	logStream.close();

	return errors;
}